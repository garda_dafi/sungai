import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// Layout
import LayoutMaster from '@/components/layout/Master';
// import LayoutNotFound from '../components/layout/NotFound';
// import LayoutForbidden from '../components/layout/Forbidden';

// Route
import Home from '@/components/Home'
import Description from '@/components/master/Description'
import SungaiMenu from '@/components/master/SungaiMenu'
import MainMenu from '@/components/master/MainMenu'

import History from '@/components/content/SejarahSungai'
import Culture from '@/components/content/ProdukBudaya'
import People from '@/components/content/WargaBicara'
import Problem from '@/components/content/Problematika'

const routes = [
    {
        path: '/',
        component: LayoutMaster,
        children: [
            {
                path: '',
                name: 'home',
                component: Home,
            },
            {
                path: 'sungai/:name',
                name: 'sungai',
                component: SungaiMenu,
                children: [
                    {
                        path: 'menu',
                        name: 'menu',
                        component: MainMenu
                    },
                    {
                        path: 'desc',
                        name: 'desc',
                        component: Description,
                        children: [
                            {
                                path: 'history',
                                name: 'history',
                                component: History
                            },
                            {
                                path: 'culture',
                                name: 'culture',
                                component: Culture
                            },
                            {
                                path: 'people',
                                name: 'people',
                                component: People
                            },
                            {
                                path: 'problem',
                                name: 'problem',
                                component: Problem
                            }
                        ]
                    }
                ]
            }
        ]
    },
    // { path: '*', component: LayoutNotFound }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
});

export default router;